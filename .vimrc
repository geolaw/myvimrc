" plugged install
" curl -fLo ~/.vim/autoload/plug.vim --create-dirs     https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
" and then plugged init and load with ^Plug ...   PlugInstall after inserting a new Plug
call plug#begin('~/.vim/plugged')
Plug 'junegunn/goyo.vim'
Plug 'https://github.com/tpope/vim-eunuch'
call plug#end()


" Some basics:  http://vimdoc.sourceforge.net/htmldoc/options.html
	set nocompatible			" make sure we get all of vim's tweaks.  compatible turns off vims tweaks
	filetype plugin on  
	syntax on					" start with syntax on - keystroke later to turn if off dynamically
	set encoding=utf-8
	set number relativenumber	" turn on line numbers and relative numbers (for quick movement ... 10j  down 10 lines, 10k up 10 lines

	set ai						" auto indent
	set cindent					" auto c style indenting
	" recommended tab settings from http://vimdoc.sourceforge.net/htmldoc/options.html
	set tabstop=8				" tab width 8 - 
	set softtabstop=4			" use spaces instead of \t
	set shiftwidth=4			" 
	set noexpandtab				"
	set smarttab				" when enabled, a tab at the beginning of a line inserts spaces from shiftwidth. tabs elsewhere use tabs

	set nowrap					"

colorscheme desert

" Enable autocompletion: turn on autocomplete for loading files, etc ... 
	set wildmode=longest,list,full

" Disables automatic commenting on newline:
	autocmd FileType * setlocal formatoptions-=c formatoptions-=r formatoptions-=o

" using some sub commands ... start with the space
let mapleader = " "

" Goyo plugin makes text more readable when writing prose:
	map <leader>f :Goyo \| set linebreak<CR>

" quick spelling on/off
	map <leader>s :set spell<CR>
	map <leader>S :set nospell<CR>

" shellcheck for editing shell scripts
	map <leader>K :!clear && shellcheck %<CR>

" quick syntax on/off
	map <leader>x :syntax on<CR>
	map <leader>X :syntax off<CR>

" copy paste with standard ctrl-c ctrl-v
noremap <C-c> "*y  :let @+=@*<CR>
map <C-p> "+P




" map \ l  to script to "lint" --
" other ideas : delete blank lines
" delete trailing spaces
"

" handle new splits  to the right and bottom ... default is above and left 
	set splitbelow splitright

" quick keys to change windows with h (left) j (down) k(up) l(right)
 map	<C-h> <C-w>h
 map	<C-j> <C-w>j
 map	<C-k> <C-w>k
 map	<C-l> <C-w>l

" credit where credit is due.
" Luke Smith
" https://youtu.be/cTBgtN-s2Zw
" https://youtu.be/E_rbfQqrm7g
" https://github.com/LukeSmithxyz/voidrice
"
" Damian Conway
" https://www.youtube.com/watch?v=aHm36-na4-4
" https://docs.google.com/file/d/0Bx3f0gFZh5Jqc0MtcUstV3BKdTQ/edit
" 